
#include<stdlib.h>
#include<stdio.h>

typedef struct student{
    char vorname[20];
    char nachname[20];
    int PLZ;
    char Strasse[50];
    int Klasse;
    struct student *previous;
    struct student *next;
}S;

//Variablen und Pointer
S *anfang=NULL;
//PHILLIP HATJE FRAGEN
//warum kann ich hier nicht den vorgänger vom anfang auf NULL setzen?
//anfang->previous==NULL;
S *hilfszeiger1;
S *hilfszeiger2;
int Listenlaenge=0;
char *temp;


//Funktionen
S* eingabe(void);
void anhaengen(S* eingabe);
void printListe(void);
void printListeBackwards(void);



//Main
int main(void){


  char yesno;
    while(1){
    printf("neuen Schüler eingeben? (y/n):\n");
    scanf("%s",temp);
        yesno=getc(stdin);
if(yesno=='y'||yesno=='Y'){
anhaengen(eingabe());
}else{if((yesno!='N')&&(yesno!='n')){printf("Fehler bei der Eingabe.\n");return EXIT_FAILURE;}else
{printListe(); printListeBackwards(); return EXIT_SUCCESS;}}
    }


return EXIT_SUCCESS;

}

S* eingabe(void){
  
    S *neuerEintrag=(S*)malloc(sizeof(S));
    if( neuerEintrag==NULL)printf("Es konnte kein Speicher alloziiert werden.\n");
printf("Vorname: ");scanf("%s",temp);
if(scanf("%s",neuerEintrag->vorname)!=1)printf("Fehler bei der Eingabe.\n");
printf("Nachname: ");scanf("%s",temp);
if(scanf("%s",neuerEintrag->nachname)!=1)printf("Fehler bei der Eingabe.\n");
printf("Postleitzahl eingeben: ");scanf("%s",temp);
if(scanf("%d",&neuerEintrag->PLZ)!=1)printf("Fehler bei der Eingabe.\n");
printf("Straße eingeben: ");scanf("%s",temp);
if(scanf("%s",neuerEintrag->Strasse)!=1)printf("Fehler bei der Eingabe.\n");
printf("Klasse eingeben: ");scanf("%s",temp);
if(scanf("%d",&neuerEintrag->Klasse)!=1)printf("Fehler bei der Eingabe.\n");
neuerEintrag->next=NULL;
neuerEintrag->previous=NULL;
return neuerEintrag;

}


void anhaengen(S* eingabe){
if(anfang==NULL){
    anfang=eingabe;
    anfang->next=NULL;
}else{

    hilfszeiger1=anfang;
   
    while(hilfszeiger1->next!=NULL)hilfszeiger1=hilfszeiger1->next;
hilfszeiger1->next=eingabe;
eingabe->previous=hilfszeiger1;
}

}


void printListe(void){
if(anfang==NULL){printf("Liste ist leer:/");}
else{
    printf("Die Liste ist forwards.\n\n");
    int elementcounter=0;
    hilfszeiger1=anfang;

   do{ 
        printf("Schüler %d\n\n",++elementcounter);
        printf("Vorname:\t%s\n",hilfszeiger1->vorname);
        printf("Nachname:\t%s\n",hilfszeiger1->nachname);
        printf("Postleitzahl:\t%d\n",hilfszeiger1->PLZ);
        printf("Straße:\t\t%s\n",hilfszeiger1->Strasse);
        printf("Klasse:\t\t%d\n\n\n",hilfszeiger1->Klasse);
        hilfszeiger1=hilfszeiger1->next;
   }while(hilfszeiger1!=NULL);


    
}

}


void printListeBackwards(void){
if(anfang==NULL){
    printf("Liste ist leer:/\n");
}else{
    printf("Die Liste ist Backwards.\n\n");
    anfang->previous=NULL;
    int elementcounter=0;
    hilfszeiger1=anfang;
    while(hilfszeiger1->next!=NULL)hilfszeiger1=hilfszeiger1->next;
    do{ printf("Schüler %d\n\n",++elementcounter);
        printf("Vorname:\t%s\n",hilfszeiger1->vorname);
        printf("Nachname:\t%s\n",hilfszeiger1->nachname);
        printf("Postleitzahl:\t%d\n",hilfszeiger1->PLZ);
        printf("Straße:\t\t%s\n",hilfszeiger1->Strasse);
        printf("Klasse:\t\t%d\n\n\n",hilfszeiger1->Klasse);
        hilfszeiger1=hilfszeiger1->previous;
   }while(hilfszeiger1!=NULL);
}

}
